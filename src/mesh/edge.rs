use super::vertex::VertexId;

type EdgeId = usize;

#[derive(Copy, Clone)]
struct Edge {
    pub v1: VertexId,
    pub v2: VertexId,
}

impl Edge {

}