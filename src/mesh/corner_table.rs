use super::corner::{Corner, CornerId};
use super::vertex::VertexId;

use std::vec::Vec;
use std::collections::HashMap;
use crate::mesh::triangle::{Triangle, TriangleId};

pub struct CornerTable {
    pub corners: Vec<Corner>,
    corners_from_vertex: HashMap<VertexId, Vec<CornerId>>
}

impl CornerTable {
    pub fn new (n_vertices: usize, n_faces: usize) -> Self {
        let corners = Vec::<Corner>::with_capacity(n_faces * 3);
        let corners_from_vertex =
            HashMap::<VertexId, Vec<CornerId>>::with_capacity(n_vertices);

        Self {
            corners,
            corners_from_vertex,
        }
    }

    pub fn add_triangle (&mut self, v1: VertexId, v2: VertexId, v3: VertexId) -> TriangleId {
        let id : TriangleId = self.corners.len() / 3;

        let t = self.get_triangle(id);

        let c1_id = t.c1;
        let c2_id = t.c2;
        let c3_id = t.c3;

        let empty : Vec<usize> = Vec::new();

        let v1_corners = self.get_corners(v1).unwrap_or(&empty);
        let v2_corners = self.get_corners(v2).unwrap_or(&empty);
        let v3_corners = self.get_corners(v3).unwrap_or(&empty);

        let get_third_if_co_facing = | first: CornerId, second: CornerId | -> Option<CornerId> {
            let t1 = self.get_corner_triangle(first).unwrap();
            let t2 = self.get_corner_triangle(second).unwrap();

            if t1 == t2 {
                let triangle = self.get_triangle(t1);
                let is_c1 = triangle.c1 != first && triangle.c1 != second;
                let is_c2 = triangle.c2 != first && triangle.c2 != second;

                Some (if is_c1 {triangle.c1} else if is_c2 {triangle.c2} else {triangle.c3})
            } else {
                None
            }
        };

        let c1_opp = v2_corners.iter().filter_map(| first| {
            v3_corners.iter().filter_map(|second| {
                get_third_if_co_facing(first.clone(), second.clone())
            }).last()
        }).last();
        let c2_opp = v1_corners.iter().filter_map(| first| {
            v3_corners.iter().filter_map(|second| {
                get_third_if_co_facing(first.clone(), second.clone())
            }).last()
        }).last();
        let c3_opp = v2_corners.iter().filter_map(| first| {
            v1_corners.iter().filter_map(|second| {
                get_third_if_co_facing(first.clone(), second.clone())
            }).last()
        }).last();

        match c1_opp {
            Some(c) => self.corners[c].o = Some(c1_id),
            None => {}
        };

        match c2_opp {
            Some(c) => self.corners[c].o = Some(c2_id),
            None => {}
        };

        match c3_opp {
            Some(c) => self.corners[c].o = Some(c3_id),
            None => {}
        };

        self.add_corner(Corner::init(v1, id, c2_id, c1_opp));
        self.add_corner(Corner::init(v2, id, c3_id, c2_opp));
        self.add_corner(Corner::init(v3, id, c1_id, c3_opp));

        return id;
    }

    fn add_corner (&mut self, corner: Corner) -> CornerId {
        let id : CornerId = self.corners.len();

        if self.corners_from_vertex.contains_key(&corner.v) {
            self.corners_from_vertex.get_mut(&corner.v).unwrap().push(id);
        } else {
            self.corners_from_vertex.insert(corner.v, vec![id]);
        }

        self.corners.push(corner);
        return id;
    }

    pub fn get_corners(&self, v: VertexId) -> Option<&Vec<CornerId>> {
        self.corners_from_vertex.get(&v)
    }

    pub fn get_corner (&self, c: CornerId) -> Option<&Corner> {
        self.corners.get(c)
    }

    pub fn get_triangle (&self, f: TriangleId) -> Triangle {
        Triangle {
            c1: (f * 3) + 0,
            c2: (f * 3) + 1,
            c3: (f * 3) + 2,
        }
    }

    pub fn get_corner_triangle (&self, c: CornerId) -> Option<TriangleId> {
        match self.get_corner(c) {
            Some(corner) => Some(corner.t),
            None => None,
        }
    }

    pub fn get_next (&self, c: CornerId) -> Option<CornerId> {
        match self.get_corner(c) {
            Some(corner) => Some(corner.n),
            None => None
        }
    }

    pub fn get_previous (&self, c: CornerId) -> Option<CornerId> {
        match self.get_corner(c) {
            Some(corner) => self.get_next(corner.n),
            None => None
        }
    }

    pub fn get_opposite (&self, c: CornerId) -> Option<CornerId> {
        match self.get_corner(c) {
            Some(corner) => corner.o,
            None => None
        }
    }

    pub fn get_right (&self, c: CornerId) -> Option<CornerId> {
        match self.get_next(c) {
            Some(corner) => self.get_opposite(corner),
            None => None
        }
    }

    pub fn get_left (&self, c: CornerId) -> Option<CornerId> {
        match self.get_previous(c) {
            Some(corner) => self.get_opposite(corner),
            None => None
        }
    }
}