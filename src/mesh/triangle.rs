use super::corner::CornerId;

pub type TriangleId = usize;

#[derive(Copy, Clone)]
pub struct Triangle {
    pub c1: CornerId,
    pub c2: CornerId,
    pub c3: CornerId,
}