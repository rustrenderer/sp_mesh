use super::vertex::VertexId;
use super::triangle::TriangleId;

pub type CornerId = usize;

#[derive(Copy, Clone)]
pub struct Corner {
    pub v: VertexId, // Vertex of the corner
    pub t: TriangleId, // Incident triangle
    pub n: CornerId, // Next corner in triangle (ccw order)
    pub o: Option<CornerId>, // Opposite corner
}

impl Corner {
    pub fn init (v: VertexId, t: TriangleId, n: CornerId, o: Option<CornerId>) -> Corner {
        Self {
            v,
            t,
            n,
            o,
        }
    }
}