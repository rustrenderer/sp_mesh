use super::vertex::VertexId;

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Face {
    pub v1: VertexId,
    pub v2: VertexId,
    pub v3: VertexId,
}

impl Face {
    pub fn new (v1: VertexId, v2: VertexId, v3: VertexId) -> Self {
        Face {v1, v2, v3}
    }
}