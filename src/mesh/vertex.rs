pub type VertexId = usize;

use sp_math::vector::{ Vector3f, Vector2f };

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Vertex {
    pub position: Vector3f,
    pub normal: Vector3f,
    pub tex_coord: Vector2f,
}

impl Vertex {
    pub fn new (position: Vector3f, normal: Vector3f, tex_coord: Vector2f) -> Self {
        Vertex {
            position,
            normal,
            tex_coord
        }
    }

    pub fn from_position (pos: Vector3f) -> Self {
        Vertex {
            position: pos,
            normal: Vector3f::from_one(0.0),
            tex_coord: Vector2f::from_one(0.0),
        }
    }
}