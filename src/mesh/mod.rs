pub mod vertex;
pub mod face;
pub mod triangle;
pub mod edge;

pub mod corner;
mod corner_table;

use std::vec::Vec;

use corner_table::CornerTable;
use face::Face;
use vertex::{Vertex, VertexId};

use sp_math::vector::Vector3f;

pub struct Mesh {
    corner_table: CornerTable,
    vertices: Vec<Vertex>,
    faces: Vec<Face>,
}

impl Mesh {
    pub fn build (vertices: Vec<Vertex>, faces: Vec<Face>) -> Self {
        let n_vertices = vertices.len();
        let n_faces = faces.len();

        let mut table = CornerTable::new(n_vertices, n_faces);

        for f in faces.iter() {
            table.add_triangle(f.v1, f.v2, f.v3);
        }

        Self {
            corner_table: table,
            vertices,
            faces
        }
    }

    pub fn get_vertices (&self) -> &Vec<Vertex> {
        &self.vertices
    }

    pub fn get_nb_vertices (&self) -> usize { self.vertices.len() }

    pub fn get_faces (&self) -> &Vec<Face> {
        &self.faces
    }

    pub fn get_nb_faces (&self) -> usize { self.faces.len() }

    pub fn get_vertex_faces (&self, vertex: VertexId) -> Vec<&Face> {
        let corners = match self.corner_table.get_corners(vertex) {
            Some(v) => v,
            None => return Vec::new()
        };

        corners.iter().map(|c| {
            &self.faces[self.corner_table.get_corner(c.clone()).unwrap().t]
        }).collect()
    }

    pub fn get_vertex_ring (&self, vertex: VertexId) -> Vec<&Vertex> {
        let corners = self.corner_table.get_corners(vertex).unwrap();

        corners.iter().map(|c| {
            let corner_id = self.corner_table.get_next(c.clone()).unwrap();
            let corner = self.corner_table.get_corner(corner_id).unwrap();

            &self.vertices[corner.v]
        }).collect()
    }

    pub fn get_face_vertices (&self, face: &Face) -> Vec<&Vertex> {
        vec![&self.vertices[face.v1], &self.vertices[face.v2], &self.vertices[face.v3]]
    }

    pub fn compute_normals (&self) -> Self {
        let mut vertices = self.vertices.clone();
        let faces = self.faces.clone();

        for v_id in 0..self.get_nb_vertices() {
            let v_faces = self.get_vertex_faces(v_id);
            let mut new_normal = Vector3f::from_one(0.0);
            for f in v_faces.iter() {
                let v1 = &vertices[f.v1].position;
                let v2 = &vertices[f.v2].position;
                let v3 = &vertices[f.v3].position;

                new_normal = new_normal +  (*v2 - *v1).cross(*v3 - *v1).normalized();
            }
            vertices[v_id].normal = new_normal / v_faces.len() as f32;
        }

        Self::build(vertices, faces)
    }
}