// Because it is a lib, we do not care about dead code,
// if enabled, it could pollute the compilation output.
#![allow(dead_code)]
pub mod mesh;
pub mod generators;

pub extern crate sp_math;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
