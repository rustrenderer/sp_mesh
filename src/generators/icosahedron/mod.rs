use crate::mesh::Mesh;
use crate::mesh::face::Face;
use crate::mesh::vertex::Vertex;
use std::collections::HashMap;
use sp_math::vector::Vector3f;

///
/// ```text
///   _                     _              _
///  (_)                   | |            | |
///   _  ___ ___  ___  __ _| |__   ___  __| |_ __ ___  _ __
///  | |/ __/ _ \/ __|/ _` | '_ \ / _ \/ _` | '__/ _ \| '_ \
///  | | (_| (_) \__ \ (_| | | | |  __/ (_| | | | (_) | | | |
///  |_|\___\___/|___/\__,_|_| |_|\___|\__,_|_|  \___/|_| |_|
///
///          _-_.
///       _-',^. `-_.
///   ._-' ,'   `.   `-_
///  !`-_._________`-':::
///  !   /\        /\::::
///  ;  /  \      /..\:::
///  ! /    \    /....\::
///  !/      \  /......\:
///  ;--.___. \/_.__.--;;
///   '-_    `:!;;;;;;;'
///      `-_, :!;;;''
///          `-!'
/// ```

pub fn make_icosahedron () -> Mesh {
    //! Returns a mesh representing an icosahedron.
    //!
    //! # Examples :
    //! ```
    //! use sp_mesh::generators::icosahedron::make_icosahedron;
    //! let icosahedron = make_icosahedron();
    //! ```


    let vertices: Vec<Vertex> = vec![
        Vertex::from_position(Vector3f::new(0.0, -0.525731, 0.850651)),
        Vertex::from_position(Vector3f::new( 0.850651, 0.0, 0.525731)),
        Vertex::from_position(Vector3f::new(0.850651, 0.0, -0.525731)),
        Vertex::from_position(Vector3f::new( -0.850651, 0.0, -0.525731)),

        Vertex::from_position(Vector3f::new(-0.850651, 0.0, 0.525731)),
        Vertex::from_position(Vector3f::new(-0.525731, 0.850651, 0.0)),
        Vertex::from_position(Vector3f::new(0.525731, 0.850651, 0.0)),
        Vertex::from_position(Vector3f::new(0.525731, -0.850651, 0.0)),

        Vertex::from_position(Vector3f::new(-0.525731, -0.850651, 0.0)),
        Vertex::from_position(Vector3f::new(0.0, -0.525731, -0.850651)),
        Vertex::from_position(Vector3f::new(0.0, 0.525731, -0.850651)),
        Vertex::from_position(Vector3f::new(0.0, 0.525731, 0.850651)),
    ];

    let faces : Vec<Face> = vec![
        Face::new(1, 2, 6),
        Face::new(1, 7, 2),
        Face::new(3, 4, 5),
        Face::new(4, 3, 8),
        Face::new(6, 5, 11),

        Face::new( 5, 6, 10),
        Face::new( 9, 10, 2),
        Face::new(10, 9, 3),
        Face::new(7, 8, 9),
        Face::new( 8, 7, 0),

        Face::new(11, 0, 1),
        Face::new(0, 11, 4),
        Face::new(6, 2, 10),
        Face::new(1, 6, 11),
        Face::new(3, 5, 10),

        Face::new(5, 4, 11),
        Face::new(2, 7, 9),
        Face::new(7, 1, 0),
        Face::new(3, 9, 8),
        Face::new(4, 8, 0),
    ];

    Mesh::build(vertices, faces)
}

pub fn refine_icosahedron (icosahedron: &Mesh, refinement: u32) -> Mesh {
    //! Returns a mesh representing a refine icosahedron from a given icosahedron.
    //!
    //! For each triangle of the input icosahedron we create four triangles as follow :
    //! ```text
    //!              v3                  v3
    //!             /  \                /  \
    //!            /    \              /    \
    //!           /      \     ==>   v6 ____ v5
    //!          /        \          / \    / \
    //!         /          \        /   \  /   \
    //!       v1 __________ v2    v1 ___ v4 ___ v2
    //! ```
    //! All the new vertices are normalized to b. By doing this, the resulted refined icosahedron
    //! will converge to a sphere.
    //!
    //! # Examples :
    //! ```
    //! use sp_mesh::generators::icosahedron::{make_icosahedron, refine_icosahedron};
    //!
    //! let icosahedron = make_icosahedron();
    //! let refined_3_times = refine_icosahedron(&icosahedron, 3);
    //! let refined_4_times = refine_icosahedron(&refined_3_times, 1);
    //! ```

    let mut vertices: Vec<Vertex> = icosahedron.get_vertices().iter().map( |v| {
        v.clone()
    }).collect();

    let mut generated_vertices : HashMap<usize, usize> = HashMap::new();

    let mut middle_point = | v1: usize, v2: usize | -> usize {
        let greater = v1.max(v2);
        let lesser = v1.min(v2);

        let key = (lesser << 32) + greater;

        if generated_vertices.contains_key(&key) {
            generated_vertices.get(&key).unwrap().clone()
        } else {
            let vertex1 = vertices[v1];
            let vertex2 = vertices[v2];

            let new_position = (vertex1.position + vertex2.position) / 2.0;
            let new_position = new_position.normalized();

            let new_normal = (vertex1.normal + vertex2.normal) / 2.0;
            let new_normal = new_normal.normalized();

            let new_tex_coord = (vertex1.tex_coord + vertex2.tex_coord) / 2.0;

            let new_vertex = Vertex::new(new_position, new_normal, new_tex_coord);

            let id = vertices.len();
            vertices.push(new_vertex);
            generated_vertices.insert(key, id);

            id
        }
    };

    let mut faces: Vec<Face> = icosahedron.get_faces().iter().map(|f| {
        f.clone()
    }).collect();

    for _ in 0..refinement {
        let mut step_faces : Vec<Face> = Vec::new();

        faces.iter().for_each(|f| {
            let v4 = middle_point(f.v1, f.v2);
            let v5 = middle_point(f.v2, f.v3);
            let v6 = middle_point(f.v1, f.v3);

            step_faces.push(Face::new(f.v1,v4,v6));
            step_faces.push(Face::new(v4,f.v2,v5));
            step_faces.push(Face::new(v4,v5,v6));
            step_faces.push(Face::new(v6,v5,f.v3));
        });

        faces = step_faces;
    }

    Mesh::build(vertices, faces)
}