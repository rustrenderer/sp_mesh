use crate::mesh::Mesh;
use crate::mesh::vertex::Vertex;
use crate::mesh::face::Face;

use super::icosahedron::{make_icosahedron, refine_icosahedron};
use sp_math::vector::{Vector3f, Vector2f};

/// ```text
///             _
///            | |
///   ___ _ __ | |__   ___ _ __ ___
///  / __| '_ \| '_ \ / _ \ '__/ _ \
///  \__ \ |_) | | | |  __/ | |  __/
///  |___/ .__/|_| |_|\___|_|  \___|
///      | |
///      |_|
///          .d$#T!!!~"#*b.
///        d$MM!!!!~~~     "h
///      dRMMM!!!~           ^k
///     $RMM!!~                ?
///    $MMM!~                   "
///   $MM!!                      9
///  4RM!!                        F
///  $!!!                         J
///  9!!~                         F
///  'k~~                        :
///   3>                         F
///    9>                       F
///     "i                    :"
///       t.                .P
///         #c.          .z#
///            ^#*heee*#"
/// ```

pub fn make_icosphere (refinement: u32) -> Mesh {
    //! Returns a mesh representing an icosphere.
    //!
    //! # Arguments :
    //!
    //! * `refinement` - Positive integer that controls the refinement level of the icosahedron.
    //!
    //! # Examples :
    //! ```
    //! use sp_mesh::generators::sphere::make_icosphere;
    //! let sphere = make_icosphere(5);
    //!
    //! // This is the same as
    //! use sp_mesh::generators::icosahedron::*;
    //! let icosahedron = make_icosahedron();
    //! let sphere = refine_icosahedron(&icosahedron, 5);
    //! ```
    refine_icosahedron(&make_icosahedron(), refinement)
}

pub fn make_uvsphere (meridians: usize, parallels: usize) -> Mesh {
    //! Returns a mesh representing an icosphere.
    //!
    //! # Arguments :
    //!
    //! * `meridians` - Positive integer that controls the number of horizontal subdivisions.
    //! * `parallels` - Positive integer that controls the number of vertical subdivisions.
    //!
    //! # Examples :
    //! ```
    //! use sp_mesh::generators::sphere::make_uvsphere;
    //! let sphere = make_uvsphere(16, 16);
    //! ```

    let pi = std::f32::consts::PI;
    let pi_double = pi * 2.0;
    let mut vertices: Vec<Vertex> = Vec::new();

    let mut add_sphere_vertex = |phi: f32, theta: f32|  {
        let cos_phi = phi.cos();
        let sin_phi = phi.sin();

        let cos_theta = theta.cos();
        let sin_theta = theta.sin();

        let x = sin_theta*cos_phi;
        let y = sin_theta*sin_phi;
        let z = cos_theta;

        let position = Vector3f::new(x, y, z);
        let uv = Vector2f::new(0.5 + z.atan2(x) / pi_double, 0.5 - y.asin() / pi);

        vertices.push(Vertex::new(position.clone(), position.normalized(), uv));
    };

    add_sphere_vertex(0f32,0f32);
    for p in 0..parallels {
        for m in 0..meridians {
            let phi = m as f32 * pi_double / meridians as f32;
            let theta = p as f32 * pi / parallels as f32;

            add_sphere_vertex(phi,theta);
        }
    }
    add_sphere_vertex(pi_double, pi);

    let mut faces : Vec<Face> = Vec::new();

    for m in 0..meridians {
        faces.push(Face::new(0, m + 1, (m + 2) % (meridians + 1)));
    }

    for p in 0..parallels - 1 {
        let first_current_parallel_index = p * meridians + 1;
        let last_current_parallel_index = first_current_parallel_index + meridians;
        let first_next_parallels_index = (p + 1) * meridians;
        let last_next_parallels_index = first_next_parallels_index + meridians;

        for m in 0..meridians {
            let top = first_current_parallel_index + m;
            let top_next = if top + 1 <= last_current_parallel_index {top + 1} else {first_current_parallel_index};
            let bottom = first_next_parallels_index + m;
            let bottom_next = if bottom + 1 <= last_next_parallels_index {bottom + 1} else {first_next_parallels_index};

            faces.push(Face::new(top, bottom, bottom_next));
            faces.push(Face::new(top, bottom_next, top_next));
        }
    }

    let last = vertices.len() - 1;
    let last_parallel_first_index = last - meridians;

    for m in 0..meridians {
        let current = last_parallel_first_index + m;
        let next = if current + 1 < last {current + 1} else {last_parallel_first_index};
        faces.push(Face::new(current, last, next));
    }

    Mesh::build(vertices, faces)
}