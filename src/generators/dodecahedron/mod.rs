use crate::mesh::Mesh;
use crate::mesh::vertex::Vertex;
use crate::mesh::face::Face;

/// ```text
///       _           _                _              _
///      | |         | |              | |            | |
///    __| | ___   __| | ___  ___ __ _| |__   ___  __| |_ __ ___  _ __
///   / _` |/ _ \ / _` |/ _ \/ __/ _` | '_ \ / _ \/ _` | '__/ _ \| '_ \
///  | (_| | (_) | (_| |  __/ (_| (_| | | | |  __/ (_| | | | (_) | | | |
///   \__,_|\___/ \__,_|\___|\___\__,_|_| |_|\___|\__,_|_|  \___/|_| |_|
///
///        _----------_,
///      ,"__         _-:,
///     /    ""--_--""...:\
///    /         |.........\
///   /          |..........\
///  /,         _'_........./:
///  ! -,    _-"   "-_... ,;;:
///  \   -_-"         "-_/;;;;
///   \   \             /;;;;'
///    \   \           /;;;;
///     '.  \         /;;;'
///       "-_\_______/;;'
/// ```

pub fn make_dodecahedron () -> Mesh {
    //! Returns a mesh representing an dodecahedron.
    //!
    //! # Examples :
    //! ```
    //! use sp_mesh::generators::dodecahedron::make_dodecahedron;
    //! let dodecahedron = make_dodecahedron();
    //! ```

    let vertices: Vec<Vertex> = Vec::new();
    let faces : Vec<Face> = Vec::new();

    Mesh::build(vertices, faces)
}