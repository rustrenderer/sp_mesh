use crate::mesh::Mesh;
use crate::mesh::vertex::Vertex;
use crate::mesh::face::Face;
use sp_math::vector::Vector3f;

/// ```text
///              _
///             | |
///    ___ _   _| |__   ___
///   / __| | | | '_ \ / _ \
///  | (__| |_| | |_) |  __/
///   \___|\__,_|_.__/ \___|
///
///
///                        _.-+.
///                   _.-""     '.
///               +:""            '.
///               J \               '.
///                L \             _.-+
///                |  '.       _.-"   |
///                J    \  _.-"       L
///                 L    +"          J
///                 +    |           |
///                  \   |          .+
///                   \  |       .-'
///                    \ |    .-'
///                     \| .-'
///                      +'
/// ```

pub fn make_cube () -> Mesh {
    //! Returns a mesh representing a cube.
    //!
    //! The cube contains 8 vertices listed as follow :
    //! ```text
    //!     .2------6
    //!   .' |    .'|
    //!  3---+--7'  |
    //!  |   |  |   |
    //!  |  ,0--+---4
    //!  |.'    | .'
    //!  1------5'
    //! ```
    //! # Examples :
    //! ```
    //! use sp_mesh::generators::cube::make_cube;
    //! let cube = make_cube();
    //! ```

    let values : [f32; 2] = [-0.5, 0.5];

    let vertices : Vec<Vertex> = values.iter().map( |x|{
        values.iter().map(|y| {
            values.iter().map( |z| {
                Vertex::from_position(Vector3f::new(*x, *y, *z))
            }).collect::<Vec<Vertex>>()
        }).flatten().collect::<Vec<Vertex>>()
    }).flatten().collect();

    // Easier to code it like that...
    let faces = vec![
        Face::new(0,1,2),
        Face::new(1,3,2),
        Face::new(1,5,3),
        Face::new(3,5,7),
        Face::new(5,4,7),
        Face::new(4,6,7),
        Face::new(2,6,0),
        Face::new(4,0,6),
        Face::new(6,2,3),
        Face::new(3,7,6),
        Face::new(5,1,0),
        Face::new(5,0,4),
    ];

    Mesh::build(vertices, faces)
}

/*
   .+------+     +------+     +------+     +------+     +------+.
 .' |    .'|    /|     /|     |      |     |\     |\    |`.    | `.
+---+--+'  |   +-+----+ |     +------+     | +----+-+   |  `+--+---+
|   |  |   |   | |    | |     |      |     | |    | |   |   |  |   |
|  ,+--+---+   | +----+-+     +------+     +-+----+ |   +---+--+   |
|.'    | .'    |/     |/      |      |      \|     \|    `. |   `. |
+------+'      +------+       +------+       +------+      `+------+
*/