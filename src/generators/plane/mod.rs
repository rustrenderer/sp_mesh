use crate::mesh::Mesh;
use crate::mesh::vertex::Vertex;
use sp_math::vector::{Vector3f, Vector2f};
use crate::mesh::face::Face;

/// ```text
///         _
///        | |
///   _ __ | | __ _ _ __   ___
///  | '_ \| |/ _` | '_ \ / _ \
///  | |_) | | (_| | | | |  __/
///  | .__/|_|\__,_|_| |_|\___|
///  | |
///  |_|            __/\__
///                `==/\==`
///      ____________/__\____________
///     /____________________________\
///       __||__||__/.--.\__||__||__
///      /__|___|___( >< )___|___|__\
///                _/`--`\_
///               (/------\)
///
/// ```

pub fn make_plane () -> Mesh {
    //! Returns a mesh representing a plane.
    //!
    //! The cube contains 4 vertices listed as follow :
    //! ```text
    //!          0 ___________________ 2
    //!          /\__                 /
    //!         /    \__             /
    //!        /        \_          /
    //!       /           \__      /
    //!      /               \__  /
    //!     1 __________________ 3
    //! ```
    //! # Examples :
    //! ```
    //! use sp_mesh::generators::plane::make_plane;
    //! let plane = make_plane();
    //! ```

    let w_values = [-0.5, 0.5];
    let d_values = [-0.5, 0.5];

    let vertices : Vec<Vertex> = w_values.iter().map( |x|{
        d_values.iter().map(|z| {
            Vertex::new(
                Vector3f::new(*x, 0.0, *z),
                Vector3f::new(0.0, 1.0, 0.0),
                Vector2f::new(*x + 0.5, *z + 0.5)
            )
        }).collect::<Vec<Vertex>>()
    }).flatten().collect();

    let faces = vec![
        Face::new(0,1,2),
        Face::new(1,3,2),
    ];

    Mesh::build(vertices, faces)
}

pub fn make_view_plane () -> Mesh {
    //! Returns a mesh representing a plane.
    //! This plane is expressed in view space to fill the viewport.
    //! It is generated with corrects normals and uv coordinates.
    //!
    //! # Examples :
    //! ```
    //! use sp_mesh::generators::plane::make_view_plane();
    //! let plane = make_view_plane();
    //! ```
    let normal = Vector3f::new(0.0, 0.0, 1.0);

    let vertices = vec![
        Vertex::new(Vector3f::new(-1.0,  1.0, 0.0), normal.clone(), Vector2f::new(0.0, 1.0)),
        Vertex::new(Vector3f::new( 1.0,  1.0, 0.0), normal.clone(), Vector2f::new(1.0, 1.0)),
        Vertex::new(Vector3f::new(-1.0, -1.0, 0.0), normal.clone(), Vector2f::new(0.0, 0.0)),
        Vertex::new(Vector3f::new( 1.0, -1.0, 0.0), normal.clone(), Vector2f::new(1.0, 0.0)),
    ];

    let faces = vec![
        Face::new(0,1,2),
        Face::new(1,3,2),
    ];

    Mesh::build(vertices, faces)
}